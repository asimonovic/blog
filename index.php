<?php
session_start();

use App\Controller\IndexController;

require 'vendor/autoload.php';

ini_set('display_errors', 1);
error_reporting(E_ALL);

$ctrl = new IndexController();
$ctrl->setProperties();
$ctrl->process();
$ctrl->display();