<?php

namespace App\Traits;

trait Mapper
{

    public function exchangeArray(array $array)
    {
        foreach ($array as $property => $value) {
            if (property_exists($this, $property)) {
                $this->$property = $value;
            }
        }
    }

    public function __call($name, $arguments)
    {
        $propertyName = str_replace([
            'get',
            'set'
        ], '', $name);

        if (empty($arguments)) {
            return $this->$propertyName;
        } else {
            $this->$propertyName = $arguments[0];
        }
    }

    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $this->$name = strtolower($value);
        }

        $method = 'set' . ucfirst($name);

        if (method_exists($this, $method)) {
            $this->$method($value);
        }
    }
}
