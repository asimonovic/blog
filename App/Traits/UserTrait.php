<?php

namespace App\Traits;

use App\Model\User;

trait UserTrait
{
    public function authenticate()
    {
        $this->isLoggedIn();

        if (empty($_POST['username']) || empty($_POST['password'])) {
            $this->toast('Please enter username and password.', 'warning');
            $this->redirect('?action=login', 303);
        } else {
            $user = $this->userDAO->findByUsername($_POST['username']);
            $count = $user->rowCount();

            if ($count == 1) {
                $user = $user->fetchObject(User::class);
                if (password_verify($_POST['password'], $user->password)) {

                    $_SESSION['id'] = $user->id;
                    $_SESSION['username'] = $user->username;
                    $_SESSION['email'] = $user->email;

                    $this->redirect('?action=paginated', 303);
                } else {
                    $this->toast('Wrong password.', 'warning');
                    $this->redirect('?action=login', 303);
                }
            } else {
                $this->toast('No user with this username.', 'warning');
                $this->redirect('?action=login', 303);
            }
        }
    }

    public function isLoggedIn()
    {
        if (isset($_SESSION['username'])) {
            $this->redirect('?action=paginated', 303);
        }
    }

    public function login()
    {
        $this->isLoggedIn();
        $this->getFileContent('login.tpl.php');
    }

    public function logout()
    {
        unset($_SESSION['id']);
        unset($_SESSION['username']);
        unset($_SESSION['email']);
        $this->redirect('?action=login', 303);
    }
}
