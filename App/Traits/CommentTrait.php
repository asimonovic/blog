<?php

namespace App\Traits;

use App\Model\Comment;

trait CommentTrait
{
    public function saveComment()
    {
        $comment = new Comment();
        $comment->exchangeArray((array)$this);

        $comment->author = $_SESSION['username'];
        $comment->creation_date = date('Y-m-d H:i:s');

        try {
            $this->commentDAO->saveComment($comment);
            $this->toast('Comment saved.', 'success');
            $this->redirect('?action=post&id=' . $comment->post_id, 303);

        } catch (\PDOException $e) {
            $this->toast($e->getMessage(), 'warning');
        }
    }
}
