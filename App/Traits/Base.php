<?php

namespace App\Traits;

trait Base
{

    protected function getFileContent($file)
    {
        ob_start();
        require $this->viewDir . $file;
        $this->content = ob_get_contents();
        ob_end_clean();
    }

    protected function manageModule()
    {
        if ($this->module) {

            $subController = ucfirst($this->module) . 'Controller';

            if (!class_exists($subController)) {
                $reflector = new \ReflectionClass(get_called_class());
                $subController = $reflector->getNamespaceName() . '\\' . $subController;

                if (!class_exists($subController)) {
                    throw new \Exception("Class {$subController} not exists!");
                }
            }

            $this->subController = new $subController();

            if ($this->subController && method_exists($this->subController, 'init')) {
                $this->subController->init();
            }
            if ($this->subController && method_exists($this->subController, 'setProperties')) {
                $this->subController->setProperties();
            }
        }
    }

    public function setProperties()
    {
        $data = array_merge($_GET, $_POST);

        foreach ($data as $k => $v) {
            $method = 'set' . $k;
            if (method_exists($this, $method)) {
                $this->$method($v);
            } else {
                $this->$k = $v;
            }
        }
    }

    public function process()
    {

        try {
            $method = $this->action;
            $this->manageModule();
            if ($this->subController && method_exists($this->subController, $method)) {
                $res = $this->subController->$method($this);
                $this->result = $res ? $res : $this->subController->result;
            } elseif (method_exists($this, $method)) {

                $resTrigger = $this->triggerAction('before' . strtolower($this->action));
                if ($resTrigger) {
                    $res = $this->$method();
                    if (!$this->result) {
                        $this->result = $res;
                    }

                    $this->triggerAction('after' . strtolower($this->action));
                }
            } else {
                throw new \Exception('Method not found' . ': ' . $method);
            }

            return $this->result;
        } catch (\Exception $e) {
            $this->result['success'] = false;
            $this->result['message'] = $e->getMessage();
            $this->result['ExceptionInfo'] = $e->getMessage() . '@' . $e->getFile() . ':' . $e->getLine();
        }
    }

    protected function triggerAction($action)
    {
        if (method_exists($this, $action)) {
            return $this->{$action}();
        }
        return true;
    }

    public function display()
    {
        require $this->viewDir . $this->tpl;
    }

    public function redirect($url, $statusCode = 303)
    {
        header('Location: ' . $url, true, $statusCode);
        die();
    }

    public function toast($message, $type)
    {
        $_SESSION['toastr'] = [
            'type' => $type,
            'message' => $message
        ];
    }
}
