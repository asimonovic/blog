<?php

namespace App\Traits;

use App\Db\Pagination;

trait PostTrait
{
    public function posts()
    {
        $this->posts = $this->postDAO->fetchAll();
        $this->getFileContent('posts.tpl.php');
    }

    public function paginated()
    {
        $paginator =  new Pagination('posts');
        $this->pagination = $paginator;
        $this->posts = $paginator->get_data();
        $this->pages = $paginator->get_pagination_number();

        $this->getFileContent('posts-paginated.tpl.php');
    }

    public function post()
    {
        if (isset($this->id)) {
            $this->comments = $this->commentDAO->getCommentsByPostId($this->id);
            $this->post = $this->postDAO->fetch($this->id);
            $this->getFileContent('post.tpl.php');
        } else {
            $this->redirect('?action=posts', 303);
        }
    }
}
