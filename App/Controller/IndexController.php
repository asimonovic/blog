<?php

namespace App\Controller;

use App\Model\UserDAO;
use App\Traits\UserTrait;
use App\Traits\PostTrait;
use App\Model\PostDAO;
use App\Model\CommentDAO;
use App\Traits\CommentTrait;

class IndexController extends BaseController
{

    public $content = '';

    public $action = 'paginated';

    public $tpl = 'layout.tpl.php';

    protected $formsDir = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Form' . DIRECTORY_SEPARATOR;

    protected $viewDir = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'View' . DIRECTORY_SEPARATOR . 'index' . DIRECTORY_SEPARATOR;

    protected $userDAO;

    protected $postDAO;

    public function __construct()
    {
        parent::__construct();

        $this->userDAO = new UserDAO($this->connection);
        $this->postDAO = new PostDAO($this->connection);
        $this->commentDAO = new CommentDAO($this->connection);
    }

    use UserTrait;

    use PostTrait;

    use CommentTrait;
}
