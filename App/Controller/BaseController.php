<?php

namespace App\Controller;

use App\Db\DatabaseConfig;
use App\Db\Database;
use App\Traits\Base;
use Exception;
use InvalidArgumentException;

class BaseController
{
    protected $baseUrl;
    protected $documentRoot;
    protected $requestUri;
    protected $queryString;
    protected $connection;
    protected $result;
    protected $module;
    protected $subController;

    public function __construct()
    {
        require 'App/Config/database.php';
        
        $databaseConfig = new DatabaseConfig($database);
        try {
            $database = new Database($databaseConfig);
            $this->connection = $database->getConnection();
        } catch (InvalidArgumentException $e) {
            die($e->getMessage());
        } catch (Exception $e) {
            die($e->getMessage());
        }

        $this->baseUrl = 'http://' . $_SERVER['SERVER_NAME'];
        $this->documentRoot = $_SERVER['DOCUMENT_ROOT'];
        $this->requestUri = $_SERVER['REQUEST_URI'];
        $this->queryString = $_SERVER['QUERY_STRING'];
    }

    use Base;
}
