<div class="container col-lg-3">

    <div class="text-center">

        <!-- Default form login -->
        <form class="text-center border border-light p-5" action="index.php" method="post">
            <input type="hidden" name="action" value="authenticate">
            <p class="h4 mb-4">Sign in</p>

            <!-- Email -->
            <input type="text" id="username" name="username" class="form-control mb-4" placeholder="Username">

            <!-- Password -->
            <input type="password" id="password" name="password" class="form-control mb-4" placeholder="Password">

            <div class="d-flex justify-content-around">
                <div>
                    <!-- Remember me -->
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="defaultLoginFormRemember">
                        <label class="custom-control-label" for="defaultLoginFormRemember">Remember me</label>
                    </div>
                </div>

            </div>

            <!-- Sign in button -->
            <button class="btn btn-blue-gradient btn-block my-4" type="submit">Sign in</button>

        </form>
        <!-- Default form login -->

    </div>
</div>


