<?php

namespace App\Model;

use App\Db\QueryBuilder;

class UserDAO
{

    private $dbTable = 'users';

    protected $connection;

    protected $queryBuilder;

    public function __construct(\PDO $connection)
    {
        $this->queryBuilder = new QueryBuilder($connection, $this->dbTable);
        $this->connection = $connection;
    }

    public function findByUsername($username)
    {
        $sql = "SELECT * FROM $this->dbTable WHERE username=:username";

        $stmt = $this->connection->prepare($sql);

        $stmt->execute([
            ":username" => $username
        ]);
        
        return $stmt;
    }

}
