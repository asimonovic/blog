<?php

namespace App\Model;

use App\Traits\Mapper;

class Post
{

    public $id;

    public $subject;

    public $body;

    public $image_name;

    public $creation_date;

    public $published;

    public $allow_comments;

    use Mapper;
}