<?php

namespace App\Model;

use App\Db\QueryBuilder;

class PostDAO
{

    private $dbTable = 'posts';

    protected $connection;

    protected $queryBuilder;

    public function __construct(\PDO $connection)
    {
        $this->queryBuilder = new QueryBuilder($connection, $this->dbTable);
        $this->connection = $connection;
    }

    public function fetch($id)
    {
        return $this->queryBuilder->fetch($id);
    }

    public function fetchAll()
    {
        return $this->queryBuilder->fetchAll();
    }

    public function savePost(Post $post)
    {
       return $this->queryBuilder->save($post);
    }

    public function updatePost(Post $post)
    {
        $this->queryBuilder->update($post);
    }

    public function deletePost($id)
    {
       return $this->queryBuilder->delete($id);
    }
}
