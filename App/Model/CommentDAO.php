<?php

namespace App\Model;

use App\Db\QueryBuilder;
use stdClass;

class CommentDAO
{

    private $dbTable = 'comments';

    protected $connection;

    protected $queryBuilder;

    public function __construct(\PDO $connection)
    {
        $this->queryBuilder = new QueryBuilder($connection, $this->dbTable);
        $this->connection = $connection;
    }

    public function getCommentsByPostId($id)
    {
        $sql = "SELECT * FROM $this->dbTable WHERE post_id=" . (int) $id;
        $stmt = $this->connection->query($sql);
        return $stmt->fetchAll();
    }


    public function fetch($id)
    {
        return $this->queryBuilder->fetch($id);
    }

    public function fetchAll()
    {
        return $this->queryBuilder->fetchAll();
    }

    public function saveComment(Comment $comment)
    {
        return $this->queryBuilder->save($comment);
    }

    public function deleteComment($id)
    {
        return $this->queryBuilder->delete($id);
    }
}
