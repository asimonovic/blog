<?php

namespace App\Model;

use App\Traits\Mapper;

class Comment
{

    public $id;

    public $body;

    public $post_id;

    public $author;

    public $creation_date;

    use Mapper;
}