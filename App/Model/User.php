<?php

namespace App\Model;

use App\Traits\Mapper;

class User
{

    public $id;

    public $username;

    public $password;

    public $email;

    use Mapper;
}
