<?php include $this->formsDir . '_login_form.tpl.php'; ?>


<?php if (isset($_SESSION['toastr'])) : ?>
    <script type="text/javascript">
        $(function () {
            toastr.options.timeOut = 6000;
            toastr.<?= $_SESSION['toastr']['type']; ?>('<?= $_SESSION['toastr']['message']; ?>');
        });
    </script>
    <?php unset($_SESSION['toastr']); ?>
<?php endif; ?>