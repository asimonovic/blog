<!-- Page content-->
<div class="container mt-5">
    <div class="row">
        <div class="col-lg-8">
            <!-- Post content-->
            <article>
                <!-- Post header-->
                <header class="mb-4">
                    <!-- Post title-->
                    <h1 class="fw-bolder mb-1"><?= $this->post->subject ?></h1>
                    <!-- Post meta content-->
                    <div class="text-muted fst-italic mb-2">Posted on <?= $this->post->creation_date ?></div>
                    <!-- Post categories-->
                    <a class="badge bg-secondary text-decoration-none link-light" href="#!">Web Design</a>
                    <a class="badge bg-secondary text-decoration-none link-light" href="#!">Freebies</a>
                </header>
                <!-- Preview image figure-->

                <figure class="mb-4"><img class="img-fluid rounded" src="<?= $this->baseUrl . '/public/images/posts/' . $this->post->image_name ?>" alt="..." /></figure>
                <!-- Post content-->
                <section class="mb-5">

                    <?= $this->post->body ?>

                </section>
            </article>

            <?php if (isset($_SESSION['username'])) : ?>
                <!-- Comments section-->
                <section class="mb-5">
                    <div class="card bg-light">

                        <div class="card-body">
                            <!-- Comment form-->
                            <form method="post" action="index.php">
                                <input type="hidden" name="action" value="saveComment">
                                <input type="hidden" name="post_id" value="<?= $this->post->id ?>">
                                <textarea class="form-control" rows="3" name="body" placeholder="Leave a comment!"></textarea>
                                <button class="btn btn-primary" type="submit" name="save" value="Save">Save comment
                            </form>
                        </div>

                        <div class="card-body">
                            <?php if ($this->comments) : ?>
                                <?php foreach ($this->comments as $comment) : ?>
                                    <!-- Single comment-->
                                    <div class="d-flex">
                                        <div class="flex-shrink-0"><img class="rounded-circle" src="https://dummyimage.com/50x50/ced4da/6c757d.jpg" alt="..." /></div>
                                        <div class="ms-3">
                                            <div class="fw-bold"><?= $comment['author']; ?></div>
                                            <?= $comment['body']; ?>
                                        </div>
                                        <p><?= $comment['creation_date']; ?></p>
                                    </div>

                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </section>

            <?php else : ?>
                <p><a href="?action=login">Login to comment</a></p>
            <?php endif; ?>
        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Search</div>
                <div class="card-body">
                    <div class="input-group">
                        <input class="form-control" type="text" placeholder="Enter search term..." aria-label="Enter search term..." aria-describedby="button-search" />
                        <button class="btn btn-primary" id="button-search" type="button">Go!</button>
                    </div>
                </div>
            </div>
            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Categories</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="#!">Web Design</a></li>
                                <li><a href="#!">HTML</a></li>
                                <li><a href="#!">Freebies</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-unstyled mb-0">
                                <li><a href="#!">JavaScript</a></li>
                                <li><a href="#!">CSS</a></li>
                                <li><a href="#!">Tutorials</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Side widget-->
            <div class="card mb-4">
                <div class="card-header">Side Widget</div>
                <div class="card-body">You can put anything you want inside of these side widgets. They are easy to use, and feature the Bootstrap 5 card component!</div>
            </div>
        </div>
    </div>
</div>