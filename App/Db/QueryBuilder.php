<?php

namespace App\Db;

use stdClass;

class QueryBuilder
{

    protected $connection;

    private $dbTable;

    public function __construct(\PDO $connection, $dbTable)
    {
        $this->dbTable = $dbTable;
        $this->connection = $connection;
    }

    public function fetch($id)
    {
        $sql = "SELECT * FROM $this->dbTable WHERE id=" . (int) $id;
        $stmt = $this->connection->query($sql);
        return $stmt->fetchObject(stdClass::class);
    }

    public function fetchAll()
    {
        $sql = "SELECT * FROM $this->dbTable";
        $stmt = $this->connection->query($sql);
        return $stmt->fetchAll(\PDO::FETCH_CLASS, stdClass::class);
    }

    public function save($recordSet)
    {
        unset($recordSet->id);

        try {
            $arrayKeys = array_keys((array) $recordSet);
            $columns = implode('`, `', $arrayKeys);

            $placeHolders = ':' . implode(',:', $arrayKeys);

            $stmt = $this->connection->prepare("INSERT INTO $this->dbTable(`$columns`) VALUES($placeHolders)");

            foreach ($recordSet as $k => $v) {
                $type = \PDO::PARAM_STR;
                $dType = gettype($v);
                switch ($dType) {
                    case 'int':
                        $type = \PDO::PARAM_INT;
                        break;
                }
                $stmt->bindParam(':' . $k, $v, $type);
                unset($v);
            }

            $stmt->execute();

            return $stmt;
        } catch (\PDOException $ex) {

            echo $ex->getMessage(); die;
        }
    }

    public function update($recordSet)
    {
        try {
            $arrayKeys = array_keys((array) $recordSet);
            unset($arrayKeys['id']);

            $updateSet = '';
            foreach ($arrayKeys as $key => $column) {
                $updateSet .= '`' . $column . '`' . '= :' . $column;
                if ($key < (count($arrayKeys) - 1)) {
                    $updateSet .= ',';
                }
            }

            $sql = "UPDATE $this->dbTable SET $updateSet WHERE id= :id";

            $stmt = $this->connection->prepare($sql);

            foreach ($recordSet as $k => &$v) {
                $type = \PDO::PARAM_STR;
                if ($k == 'id') {
                    $v = (int) $v;
                    $type = \PDO::PARAM_INT;
                }
                $stmt->bindParam(':' . $k, $v, $type);
            }

            $stmt->execute();
        } catch (\PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function delete($id)
    {
        $query = "DELETE FROM $this->dbTable WHERE id =:id";
        try {
            $stmt = $this->connection->prepare($query);
            $stmt->execute([
                ":id" => (int) $id
            ]);
            return $stmt;
        } catch (\PDOException $ex) {
            echo $ex->getMessage();
        }
    }
}
