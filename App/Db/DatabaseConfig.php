<?php
namespace App\Db;

use App\Config\DbConfig;

class DatabaseConfig extends DbConfig
{

    public function getConnection()
    {
        return $this->connection;
    }

    public function __construct(array $dbConfig)
    {
        foreach ($dbConfig as $k => $v) {
            $this->$k = $v;
        }
    }
}
