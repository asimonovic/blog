<?php


use Phinx\Migration\AbstractMigration;

class CreateUsersTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $users = $this->table('users');
        $users->addColumn('username', 'string', ['limit' => 55])
            ->addColumn('password', 'string', ['limit' => 255])
            ->addColumn('token', 'string', ['limit' => 255])
            ->addColumn('email', 'string', ['limit' => 55])
            ->addColumn('first_name', 'string', ['limit' => 55, 'null' => true])
            ->addColumn('last_name', 'string', ['limit' => 55, 'null' => true])
            ->addColumn('bio', 'text', ['null' => true])
            ->addColumn('phone_number', 'string', ['limit' => 55, 'null' => true])
            ->addColumn('role', 'string', ['limit' => 55])
            ->addColumn('registration_date', 'datetime')
            ->addColumn('last_access', 'datetime', ['null' => true])
            ->addColumn('failed_auth_attempts', 'integer', ['limit' => 11, 'null' => true])
            ->addColumn('active', 'integer', ['limit' => 11])
            ->addColumn('last_access_ip', 'string', ['limit' => 55, 'null' => true])
            ->addColumn('avatar', 'string', ['limit' => 255, 'null' => true])
            ->addIndex(['username', 'email'], ['unique' => true])
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
