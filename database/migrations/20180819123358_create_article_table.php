<?php


use Phinx\Migration\AbstractMigration;

class CreateArticleTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $article = $this->table('posts');
        $article->addColumn('subject', 'string', ['limit' => 255])
            ->addColumn('body', 'text')
            ->addColumn('image_name', 'string', ['limit' => 455])
            ->addColumn('creation_date', 'datetime')
            ->addColumn('published', 'integer', ['limit' => 11])
            ->addColumn('allow_comments', 'integer', ['limit' => 11])
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('posts');
    }
}
