<?php

use Phinx\Migration\AbstractMigration;

class CreateCommentsTable extends AbstractMigration
{
       /**
     * Migrate Up.
     */
    public function up()
    {
        $article = $this->table('comments');
        $article->addColumn('body', 'text')
            ->addColumn('post_id', 'integer', ['limit' => 11])
            ->addColumn('author', 'string', ['limit' => 55])
            ->addColumn('creation_date', 'datetime')
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('comments');
    }
}

