<?php


use Phinx\Seed\AbstractSeed;

class CommentsSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $data = [
            'id' => 1,
            'body' => 'this is the first comment !',
            'post_id' => '1',
            'author' => 'admin',
            'creation_date' => date('Y-m-d H:i:s')
        ];

        $table = $this->table('comments');
        $table->insert($data);
        $table->save();
    }
}
