<?php


use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            'id' => 1,
            'username' => 'admin',
            'password' => '$2b$10$5VJmi2rUaFQSzZugXYkrzO3mB/cBCVuHKFZuOjsjjHbal75qoJVLe',
            'token' => '',
            'email' => 'admin@email.com',
            'first_name' => 'Admin',
            'last_name' => 'Admin',
            'bio' => '',
            'phone_number' => '',
            'role' => 'administrator',
            'registration_date' => date('Y-m-d H:i:s'),
            'last_access' => date('Y-m-d H:i:s'),
            'failed_auth_attempts' => 0,
            'active' => 1,
            'last_access_ip' => '',
            'avatar' => '',
        ];

        $table = $this->table('users');
        $table->insert($data);
        $table->save();
    }
}
